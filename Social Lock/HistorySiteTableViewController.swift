//
//  HistorySiteTableViewController.swift
//  Social Lock
//
//  Created by User543 on 16.06.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit
import CoreData

class HistorySiteTableViewController: UITableViewController {


    // Allow Delegate Data Core
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    // Initialization of HistorySiteCoreData
    
    var sites: [HistorySite] = []

    @IBAction func cancelButton(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getData()
        tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return sites.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HistorySiteTableViewCell", for: indexPath)
        // Configure the cell...
        let site = sites[indexPath.row]
        cell.textLabel?.text = site.webSite
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if sites[indexPath.row].webSite != nil {
            let webAddress = "http://" + sites[indexPath.row].webSite!
            let urlAddress = URL(string: webAddress)
            let urlRequest = URLRequest(url: urlAddress!)
            let privateWebViewVC : PrivateWebViewController = self.navigationController?.presentingViewController as! PrivateWebViewController
            privateWebViewVC.webView.loadRequest(urlRequest)
            privateWebViewVC.webViewDidFinishLoad(privateWebViewVC.webView)
            dismiss(animated: true, completion: nil)
        }
    }
    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            let site = sites[indexPath.row]
            context.delete(site)
            (UIApplication.shared.delegate as! AppDelegate).saveContext()
            do {
                sites = try context.fetch(HistorySite.fetchRequest())
            } catch {
                print("Неудачная загрузка файлов из CoreData")
            }
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }

    // MARK: getData function of CoreData
    func getData() {
        do {
            sites = try context.fetch(HistorySite.fetchRequest())
            sites = sites.reversed()
        } catch {
            print("Неудачная загрузка файлов из CoreData")
        }
    }
}
