//
//  PageViewController.swift
//  Social Lock
//
//  Created by User543 on 12.06.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit

class PageViewController: UIPageViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {

    var arrPagePhoto = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        arrPagePhoto = ["slide-1", "slide-2", "slide-3"];
        
        self.dataSource = self
        self.delegate = self
    
        self.setViewControllers([getViewControllerAtIndex(index: 0)] as [UIViewController], direction: UIPageViewControllerNavigationDirection.forward, animated: false, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK:- UIPageViewControllerDataSource Methods
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        let slideViewController: SlideViewController = viewController as!  SlideViewController
        
        var index = slideViewController.pageIndex
        
        if ((index == 0) || (index == NSNotFound)) {
            return nil
        }
        
        index = index - 1
        
        return getViewControllerAtIndex(index: index)
        
    }

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let slideViewController: SlideViewController = viewController as! SlideViewController
        
        var index = slideViewController.pageIndex
        
        if (index == NSNotFound) {
            return nil
        }
        
        index = index + 1
        if (index ==  arrPagePhoto.count) {
            return nil
        }
        
        return getViewControllerAtIndex(index: index)
    }
    
    // MARK:- UIPageViewControllerDelegate Methods
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return arrPagePhoto.count
    }
    
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        if let identifier = viewControllers?.first?.restorationIdentifier {
            if let index = arrPagePhoto.index(of: identifier) {
                return index
            }
        }
        return 0
    }
    
    // MARK:- Other Methods
    
    func getViewControllerAtIndex(index: Int) -> SlideViewController {
        // Create a new view controller and pass suitable data.
        let slideViewController = self.storyboard?.instantiateViewController(withIdentifier: "SlideView") as! SlideViewController
        
        slideViewController.strPhotoName = "\(arrPagePhoto[index])"
        slideViewController.pageIndex = index
        
        return slideViewController
    }
}
