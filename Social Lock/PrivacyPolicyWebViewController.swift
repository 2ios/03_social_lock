//
//  PrivacyPolicyWebViewController.swift
//  Social Lock
//
//  Created by User543 on 10.07.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit

class PrivacyPolicyWebViewController: UIViewController, UIWebViewDelegate {

    var urlAddress: String = ""

    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let url = URL(string: urlAddress)
        let urlRequest = URLRequest(url: url!)
        webView.loadRequest(urlRequest)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
