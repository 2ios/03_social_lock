//
//  PrivateWebViewController.swift
//  Social Lock
//
//  Created by User543 on 16.06.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit
import GoogleMobileAds

class PrivateWebViewController: UIViewController, UIWebViewDelegate, UISearchBarDelegate, GADBannerViewDelegate {

    @IBOutlet weak var bannerHeight: NSLayoutConstraint!
    
    // Allow Delegate Data Core
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    // Initialization of HistorySiteCoreData
    
    var sites: [HistorySite] = []
    let notificationName = Notification.Name("UnlockAction")
    
    @IBOutlet weak var gadView: GADBannerView!
    @IBOutlet weak var gadViewButton: UIButton!
    
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    @IBAction func removeAdsButton(_ sender: UIButton) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ShowRemoveAds"), object: nil)
    }
    
    @IBAction func backMenu(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func goBack(_ sender: UIBarButtonItem) {
        webView.goBack()
    }
    @IBAction func goForward(_ sender: UIBarButtonItem) {
        webView.goForward()
    }
    @IBAction func reloadWeb(_ sender: UIBarButtonItem) {
        webView.reload()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let request = GADRequest()
        request.testDevices = [kGADSimulatorID, "8f8c2fd6b38f74b1348d68cc7ac92234","865e6ef02d21d31fd38fdca391e839ab"]
        // MARK: Google Ads Key
        gadView.adUnitID = "ca-app-pub-3940256099942544/2934735716"
        gadView.rootViewController = self
        gadView.delegate = self
        gadView.load(GADRequest())
        
        NotificationCenter.default.addObserver(self, selector: #selector(checkSubscription), name: NSNotification.Name(rawValue: "CheckSubscription"), object: nil)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        checkSubscription()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        if UI_USER_INTERFACE_IDIOM() == .pad {
            self.bannerHeight.constant = 90
        }
    }
    
    func checkSubscription() {
        if StoreManager.shared.isPurchased(id: "\(StoreManager.shared.nonConsumablesProductsIds.first!)") {
            print("Product is purchased")
            hideAds()
        } else {
            print("Product is not purchased")
            showAds()
        }
    }
    
    func showAds() {
        gadView.isHidden = false
        gadViewButton.isHidden = false
    }
    
    func hideAds() {
        gadView.isHidden = true
        gadViewButton.isHidden = true
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        if let webURL = webView.request?.url?.absoluteString {
            searchBar.text = webURL
            checkIfExist(str: webURL)
            let site = HistorySite(context: context)
            if (webView.request?.url?.host?.contains("www."))! {
                let index = webView.request?.url?.host?.index((webView.request?.url?.host?.startIndex)!, offsetBy: 4)
                site.webSite = webView.request?.url?.host?.substring(from: index!)
            } else {
                site.webSite = webView.request?.url?.host!
            }
            (UIApplication.shared.delegate as! AppDelegate).saveContext()
        }
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        let err: NSError = error as NSError
        let errStr = String(err.domain)!
        if errStr == "NSURLErrorDomain" {
            let str: String = searchBar.text!.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            let url = URL(string: "http://www.google.com/search?q=\(str)")
            let urlRequest = URLRequest(url: url!)
            self.webView.loadRequest(urlRequest)
        }
    }
    
    func checkIfExist(str: String) {
        getData()
        
        var deletedIndex = 0
        for site in 0..<sites.count {
                if site > sites.endIndex-1{
                    if str.contains(sites[site-deletedIndex].webSite!) {
                    let web = sites[site-deletedIndex]
                    context.delete(web)
                    sites.remove(at: site-deletedIndex)
                    deletedIndex += 1
                    }
                }
                else{
                    if str.contains(sites[site].webSite!) {
                    let web = sites[site]
                    context.delete(web)
                    sites.remove(at: site)
                    deletedIndex += 1
                    }
                }
            }
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(true, animated: true)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if searchBar.text != nil {
            var str: String = ""
            if ((searchBar.text?.lowercased().contains("http://"))! || (searchBar.text?.lowercased().contains("https://"))!) {
                str = searchBar.text!.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            } else {
                str = "http://" + searchBar.text!.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            }
            let url = URL(string: str)
            let urlRequest = URLRequest(url: url!)
            self.webView.loadRequest(urlRequest)
        }
        searchBar.endEditing(true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
        searchBar.endEditing(true)
    }
    
    func searchBarResultsListButtonClicked(_ searchBar: UISearchBar) {
        performSegue(withIdentifier: "ToHistorySiteTableViewController", sender: nil)
    }
    
    func delay() {
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(500) ) {
        }
    }
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
//        gadViewButton.isHidden = false
    }

    // MARK: getData function of CoreData
    func getData() {
        do {
            sites = try context.fetch(HistorySite.fetchRequest())
        } catch {
            print("Неудачная загрузка файлов из CoreData")
        }
    }
}
