//
//  SettingsTableViewController.swift
//  Social Lock
//
//  Created by User543 on 09.06.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit
import CoreData

class SettingsTableViewController: UITableViewController {
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    var addressURL: String = ""
    
    @IBAction func backButton(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(showPin), name: NSNotification.Name(rawValue: "ShowPinNotification"), object: nil)
        UserDefaults.standard.set(false, forKey: "AppIntroduction")
        
        tableView.backgroundView = UIImageView(image: #imageLiteral(resourceName: "backgroundTableView"))
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillLayoutSubviews() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 40
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let viewHeader = UIView()
        viewHeader.backgroundColor = UIColor.clear
        let textLabel0 = UILabel()
        textLabel0.textColor = UIColor.white
        textLabel0.textAlignment = .center
        textLabel0.text = "Settings"
        
//        for family: String in UIFont.familyNames
//        {
//            print("\(family)")
//            for names: String in UIFont.fontNames(forFamilyName: family)
//            {
//                print("== \(names)")
//            }
//        }
        
        if UI_USER_INTERFACE_IDIOM() == .pad {
            textLabel0.font = UIFont(name: "SFProDisplay-Regular", size: 40.0)
        } else {
            textLabel0.font = UIFont(name: "SFProDisplay-Regular", size: 20.0)
        }
        textLabel0.frame = CGRect(x: 0, y: 0, width: self.tableView.frame.size.width, height: 40)
        viewHeader.addSubview(textLabel0)
        return viewHeader
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
            return 5
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            // Configure the cell...
            let settingsCell = tableView.dequeueReusableCell(withIdentifier: "SettingsTableViewCell", for: indexPath)
            settingsCell.backgroundView = UIImageView(image: #imageLiteral(resourceName: "settingsMenuCellBackground"))
            switch indexPath.row {
            case 0:
                settingsCell.textLabel?.text = "Change Passcode"
                settingsCell.imageView?.image = #imageLiteral(resourceName: "changePasswordIcon")
            case 1:
                settingsCell.textLabel?.text = "Log out, Clear Caches & Cookies"
                settingsCell.imageView?.image = #imageLiteral(resourceName: "logOutIcon")
            case 2:
                settingsCell.textLabel?.text = "App Introduction"
                settingsCell.imageView?.image = #imageLiteral(resourceName: "appIntroductionIcon")
            case 3:
                settingsCell.textLabel?.text = "PRO version"
                settingsCell.imageView?.image = #imageLiteral(resourceName: "restoreProPurchaseIcon")
            case 4:
                settingsCell.textLabel?.text = "Restore purchase"
                settingsCell.imageView?.image = #imageLiteral(resourceName: "restoreProPurchaseIcon")
            default:
                break
            }
            return settingsCell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
            switch indexPath.row {
            case 0:
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ShowPinNotification"), object: nil, userInfo:["passwordMode": PasswordMode.Change])
            case 1:
                URLCache.shared.removeAllCachedResponses()
                URLCache.shared.diskCapacity = 0
                URLCache.shared.memoryCapacity = 0
                if let cookies = HTTPCookieStorage.shared.cookies {
                    for cookie in cookies {
                        NSLog("\(cookie)")
                    }
                }
                let storage = HTTPCookieStorage.shared
                for cookie in storage.cookies! {
                    storage.deleteCookie(cookie)
                }
                deleteAllCoreData()
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ShowAlertWithOkButton"), object: nil, userInfo: ["title" : "Successfully Logged Out", "message" : "Cleared Caches & Cookies and Logged Out of All The Social Networks", "titleAction" : "OK"])
            case 2:
                UserDefaults.standard.set(true, forKey: "AppIntroduction")
                performSegue(withIdentifier: "ToPageViewControllerFromSettings", sender: nil)
            case 3:
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ShowWaiting"), object: nil)
                self.authenticateUser(bool: true)
            case 4:
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ShowWaiting"), object: nil)
//                StoreManager.shared.restoreAllPurchases()
                self.authenticateUser(bool: false)
            default:
                break
            }
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ToPrivacyPolicy" {
            let webVC:PrivacyPolicyWebViewController = segue.destination as! PrivacyPolicyWebViewController
            webVC.urlAddress = addressURL
        }
    }
    
    func deleteAllCoreData() {
            
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        //create a fetch request, telling it about the entity
        let fetchRequest: NSFetchRequest<HistorySite> = HistorySite.fetchRequest()
            
        do {
            //go get the results
            let sites = try context.fetch(fetchRequest)
                
            //You need to convert to NSManagedObject to use 'for' loops
            for site in sites as [NSManagedObject] {
                //get the Key Value pairs (although there may be a better way to do that...
                context.delete(site)
            }
            //save the context
            (UIApplication.shared.delegate as! AppDelegate).saveContext()
                
        } catch {
                print("Error with request: \(error)")
        }
    }
}

