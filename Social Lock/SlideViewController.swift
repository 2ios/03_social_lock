//
//  SlideViewController1.swift
//  Social Lock
//
//  Created by User543 on 12.06.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit

class SlideViewController: UIViewController {

    var pageIndex: Int = 0
    var strPhotoName: String!
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var getStartedButton: UIButton!
    @IBOutlet weak var getBackButton: UIButton!
    
    @IBAction func actionGetStartedButton(_ sender: UIButton) {
        if UserDefaults.standard.bool(forKey: "AppIntroduction") {
            self.dismiss(animated: false, completion: {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ShowPinNotification"), object: nil, userInfo:["passwordMode": PasswordMode.Check])
                
            })
        } else {
            self.dismiss(animated: false, completion: {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ShowPinNotification"), object: nil, userInfo:["passwordMode": PasswordMode.New])
                
            })
        }
    }

    @IBAction func getBackButton(_ sender: UIButton) {
        if UserDefaults.standard.bool(forKey: "AppIntroduction") {
            self.dismiss(animated: false, completion: {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ShowPinNotification"), object: nil, userInfo:["passwordMode": PasswordMode.Check])
                
            })
        } else {
            self.dismiss(animated: false, completion: {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ShowPinNotification"), object: nil, userInfo:["passwordMode": PasswordMode.New])
                
            })
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let imagePath = Bundle.main.path(forResource: strPhotoName, ofType: "png")
        imageView.image = UIImage(contentsOfFile: imagePath!)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if pageIndex == 2 {
            getStartedButton.isHidden = false
            if UserDefaults.standard.bool(forKey: "AppIntroduction") {
                getBackButton.isHidden = false
            } else {
                getBackButton.isHidden = true
            }
        } else {
            getStartedButton.isHidden = true
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
