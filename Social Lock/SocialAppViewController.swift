//
//  SocialAppViewController.swift
//  Social Lock
//
//  Created by User543 on 09.06.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit
import GoogleMobileAds
import SwiftyGif
import LocalAuthentication

class SocialAppViewController: UIViewController, GADBannerViewDelegate {
    
    @IBOutlet weak var bannerHeight: NSLayoutConstraint!
    
    var addressURL: String = ""
    var showFirstPin: Bool = false
    var timer : Timer?
    var alert: UIAlertController? = nil

    
    @IBOutlet weak var gadView: GADBannerView!
    @IBOutlet weak var gadViewButton: UIButton!
    @IBOutlet weak var adsImageView: UIImageView!
    @IBOutlet weak var buttonAdsImageView: UIImageView!
    
    @IBOutlet weak var socialAppView: UIView!
    
    @IBOutlet weak var socialButton0: UIButton!
    @IBOutlet weak var socialButton1: UIButton!
    @IBOutlet weak var socialButton2: UIButton!
    @IBOutlet weak var socialButton3: UIButton!
    @IBOutlet weak var socialButton4: UIButton!
    @IBOutlet weak var socialButton5: UIButton!
    @IBOutlet weak var socialButton6: UIButton!
    @IBOutlet weak var socialButton7: UIButton!
    @IBOutlet weak var socialButton8: UIButton!
    @IBOutlet weak var socialButton9: UIButton!
    @IBOutlet weak var socialButton10: UIButton!
    @IBOutlet weak var socialButton11: UIButton!
    @IBOutlet weak var socialButton12: UIButton!
    @IBOutlet weak var socialButton13: UIButton!
    @IBOutlet weak var socialButton14: UIButton!
    @IBOutlet weak var socialButton15: UIButton!
    @IBOutlet weak var socialButton16: UIButton!
    @IBOutlet weak var socialButton17: UIButton!
    @IBOutlet weak var socialButton18: UIButton!
    @IBOutlet weak var socialButton19: UIButton!
    
    @IBAction func removeAdsButton(_ sender: UIButton) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ShowRemoveAds"), object: nil)
    }
    
    @IBAction func tapSocialButton(_ sender: UIButton) {
        switch sender.tag {
        case 0:
            addressURL = "https://facebook.com"
        case 1:
            addressURL = "https://www.messenger.com"
        case 2:
            addressURL = "https://twitter.com"
        case 3:
            addressURL = "https://myspace.com"
        case 4:
            addressURL = "https://www.linkedin.com"
        case 5:
            addressURL = "https://plus.google.com"
        case 6:
            addressURL = "https://www.youtube.com"
        case 7:
            addressURL = "https://www.pinterest.com"
        case 8:
            addressURL = "https://www.tumblr.com"
        case 9:
            addressURL = "https://im.qq.com"
        case 10:
            addressURL = "https://www.flickr.com"
        case 11:
            addressURL = "https://www.instagram.com"
        case 12:
            addressURL = "https://www.okcupid.com"
        case 13:
            addressURL = "https://www.pof.com"
        case 14:
            addressURL = "http://www.match.com"
        case 15:
            addressURL = "http://www.tagged.com"
        case 16:
            addressURL = "https://www.blogger.com"
        case 17:
            addressURL = "https://www.reddit.com"
        case 18:
            performSegue(withIdentifier: "ToPrivateWebViewController", sender: nil)
        case 19:
            performSegue(withIdentifier: "ToSettingsTableViewController", sender: nil)
        default:
            addressURL = ""
        }
        performSegue(withIdentifier: "ToWebViewController", sender: nil)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ToWebViewController" {
            let webVC:WebViewController = segue.destination as! WebViewController
            webVC.urlAddress = addressURL
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.

        NotificationCenter.default.addObserver(self, selector: #selector(showPin), name: NSNotification.Name(rawValue: "ShowPinNotification"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(removeAds), name: NSNotification.Name(rawValue: "ShowRemoveAds"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(alertWithOkButton), name: NSNotification.Name(rawValue: "ShowAlertWithOkButton"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(showWaiting), name: NSNotification.Name(rawValue: "ShowWaiting"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(checkSubscription), name: NSNotification.Name(rawValue: "CheckSubscription"), object: nil)
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "CatchNotification"), object: nil, queue: nil, using: catchNotification(notification:))
        
        showFirstPin = true
        UserDefaults.standard.set(false, forKey: "ChangePass")
        
//        let request = GADRequest()
//        request.testDevices = [kGADSimulatorID, "8f8c2fd6b38f74b1348d68cc7ac92234","865e6ef02d21d31fd38fdca391e839ab"]
//        // MARK: Google Ads Key
//        gadView.adUnitID = "ca-app-pub-3940256099942544/2934735716"
//        gadView.rootViewController = self
//        gadView.delegate = self
//        gadView.load(GADRequest())
    
        let imageView = UIImageView(image: #imageLiteral(resourceName: "backgroundSocialAppView"))
        imageView.frame.size.width = self.view.frame.width
        imageView.frame.size.height = self.view.frame.height
        self.socialAppView.backgroundColor = UIColor.clear
        self.view.addSubview(imageView)
        self.view.sendSubview(toBack: imageView)
        
        //Start the StoreManager Class
        StoreManager.shared.setup()
        
//        _ = Timer.scheduledTimer(withTimeInterval: 2, repeats: true, block: { (time) in
//            NotificationCenter.default.post(name: Notification.Name("CheckSubscription"), object: nil)
//        })
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let gifmanager = SwiftyGifManager.defaultManager
        let gif = UIImage(gifName: "test.gif")
        self.adsImageView.setGifImage(gif, manager: gifmanager, loopCount: -1)
        
        checkSubscription()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if UserDefaults.standard.object(forKey: "Launched") != nil {
            print("Not first launched")
        } else {
            UserDefaults.standard.set(true, forKey: "Launched")
            print("First Launched")
            self.performSegue(withIdentifier: "ToPageViewController", sender: nil)
        }
        if showFirstPin {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ShowPinNotification"), object: nil, userInfo:["passwordMode": PasswordMode.Check])
            showFirstPin = false
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewDidLayoutSubviews() {
        if UI_USER_INTERFACE_IDIOM() == .pad {
            self.bannerHeight.constant = 90
        }
    }
    
    
    func checkSubscription() {
        if StoreManager.shared.isPurchased(id: "\(StoreManager.shared.nonConsumablesProductsIds.first!)") {
            print("Product is purchased")
            hideAds()
        } else {
            print("Product is not purchased")
            showAds()
        }
    }
    
    func showAds() {
//        gadView.isHidden = false
//        gadViewButton.isHidden = false
        if StoreFinder.userRatedApp(){
            let image1:UIImage = #imageLiteral(resourceName: "try1.tiff")
            let image2:UIImage = #imageLiteral(resourceName: "try2.tiff")
            let image3:UIImage = #imageLiteral(resourceName: "try3.tiff")
            let image4:UIImage = #imageLiteral(resourceName: "try4.tiff")
            let images = [image1, image2, image3, image4]
            let animatedImage = UIImage.animatedImage(with: images, duration: 1)
            buttonAdsImageView.image = animatedImage
            
            adsImageView.isHidden = false
            buttonAdsImageView.isHidden = false
            view.addSubview(adsImageView)
            view.bringSubview(toFront: adsImageView)
            adsImageView.addSubview(buttonAdsImageView)
            adsImageView.bringSubview(toFront: buttonAdsImageView)
//            view.addSubview(gadView)
//            view.bringSubview(toFront: gadView)
//            view.addSubview(gadViewButton)
//            view.bringSubview(toFront: gadViewButton)
            addGesture()
        } else {
            adsImageView.isHidden = true
            buttonAdsImageView.isHidden = true
            SwiftyAd.shared.showInterstitial(from: self)
        }
    }
    
    func hideAds() {
//        gadView.isHidden = true
//        gadViewButton.isHidden = true
        adsImageView.isHidden = true
        SwiftyAd.shared.removeBanner()
    }
    
    func delay() {
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(500) ) {
        }
    }
    
//    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
//        gadViewButton.isHidden = false
//    }
    
    func addGesture() {
        let gesture = UITapGestureRecognizer(target: self, action: #selector(goToBuy))
        adsImageView.addGestureRecognizer(gesture)
        adsImageView.isUserInteractionEnabled = true
    }
    
    func goToBuy() {
        self.authenticateUser(bool: true)
    }
    func showWaiting() -> Void {
        if alert == nil {
            alert = UIAlertController(title: "", message: "Please wait\n\n\n", preferredStyle: .alert)
            let spinner = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
            spinner.center = CGPoint(x: 135.0, y: 65.5)
            spinner.color = UIColor.black
            spinner.startAnimating()
            alert?.view.addSubview(spinner)
        }
        let topVC = self.getCurrentViewController(self)!
        topVC.present(alert!, animated: true, completion: nil)
    }
}

extension UIViewController {
    
    func showPin(_ note: Notification) -> Void {
        var topVC = self
        if (self.presentedViewController != nil) {
            if (self.presentedViewController is StartViewController) || (self.presentedViewController is PageViewController) {
                return
            } else {
                topVC = self.getCurrentViewController(self)!
            }
        }
        let pinVC: StartViewController = self.storyboard?.instantiateViewController(withIdentifier: "PinVC") as! StartViewController
        if let mode = note.userInfo?["passwordMode"] as? PasswordMode {
            pinVC.passwordMode = mode
        }
        topVC.present(pinVC, animated: false, completion: nil);
    }
    
    func getCurrentViewController(_ vc: UIViewController) -> UIViewController? {
        if let pvc = vc.presentedViewController {
            return getCurrentViewController(pvc)
        }
        else if let svc = vc as? UISplitViewController, svc.viewControllers.count > 0 {
            return getCurrentViewController(svc.viewControllers.last!)
        }
        else if let nc = vc as? UINavigationController, nc.viewControllers.count > 0 {
            return getCurrentViewController(nc.topViewController!)
        }
        else if let tbc = vc as? UITabBarController {
            if let svc = tbc.selectedViewController {
                return getCurrentViewController(svc)
            }
        }
        return vc
    }
    
    func removeAds() {
        let alert = UIAlertController(title: "Sorry", message: "You don't have PRO version", preferredStyle: .alert)
        let actionBuy = UIAlertAction(title: "Remove ads", style: .default) { (actionBuy) in
          NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ShowWaiting"), object: nil)
            self.authenticateUser(bool: true)
        }
        let actionRestore = UIAlertAction(title: "Restore PRO version", style: .default) { (actionRestore) in
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ShowWaiting"), object: nil)
            self.authenticateUser(bool: false)
        }
        let actionCancel = UIAlertAction(title: "Cancel", style: .cancel) { (actionCancel) in
            alert.dismiss(animated: true, completion: nil)
        }
        alert.addAction(actionBuy)
        alert.addAction(actionRestore)
        alert.addAction(actionCancel)
        let topVC = self.getCurrentViewController(self)!
        topVC.present(alert, animated: true, completion: nil)
    }
    
    func alertWithOkButton(_ note: NSNotification) -> Void {
        let alert = UIAlertController(title: note.userInfo?["title"] as? String, message: note.userInfo?["message"] as? String, preferredStyle: .alert)
        let actionOK = UIAlertAction(title: note.userInfo?["titleAction"] as? String, style: .cancel, handler: nil)
        alert.addAction(actionOK)
        let topVC = self.getCurrentViewController(self)!
        topVC.present(alert, animated: true, completion: nil)
    }
    
    func catchNotification(notification:Notification) -> Void {

        guard let userInfo = notification.userInfo,
            let title = userInfo["title"] as? String,
            let message  = userInfo["message"] as? String else {
                print("No userInfo found in notification")
                return
        }
        var topVC = self.getCurrentViewController(self)!
        if topVC .isKind(of: UIAlertController.self) {
            topVC.dismiss(animated: true, completion: {
                let alert = UIAlertController(title: "\(title)",
                                              message:"\(message)",
                    preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
                topVC = self.getCurrentViewController(self)!
                topVC.present(alert, animated: true, completion: nil)
            })
        }
    }
    
    func authenticateUser(bool: Bool) {
        let context = LAContext()
        var error: NSError?
        if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) {
            let reason = "Identify yourself!"
            context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reason) {
                (success, authenticationError) in
                DispatchQueue.main.async {
                    if success {
                        self.buyAndRestore(bool: bool)
                        print("SUCCES")
                    } else {
                        switch authenticationError! {
                        case LAError.authenticationFailed:
                            print("Failed to verify your identity.")
                            NotificationCenter.default.post(name:NSNotification.Name(rawValue: "CatchNotification"),
                                                            object: nil,
                                                            userInfo:["title":"Sorry", "message": "Failed to verify your identity!"])
                        case LAError.userCancel:
                            print("Cancel was pressed.")
                            NotificationCenter.default.post(name:NSNotification.Name(rawValue: "CatchNotification"),
                                                            object: nil,
                                                            userInfo:["title":"Sorry", "message": "Cancel was pressed!"])
                        case LAError.userFallback:
                            print("Password was pressed.")
                                self.buyAndRestore(bool: bool)
                        default:
                            print("Looks like Touch ID may not be configured")
                            NotificationCenter.default.post(name:NSNotification.Name(rawValue: "CatchNotification"),
                                                            object: nil,
                                                            userInfo:["title":"Sorry", "message": "Looks like Touch ID may not be configured!"])
                        }
                        print("FAILED")
                    }
                }
            }
        } else {
            buyAndRestore(bool: bool)
        }
    }
                        
    func buyAndRestore(bool: Bool) {
        if bool {
            if StoreManager.shared.productsFromStore.count > 0 {
                let product  = StoreManager.shared.productsFromStore[0]
                StoreManager.shared.buy(product: product)
            } else {
                NotificationCenter.default.post(name:NSNotification.Name(rawValue: "CatchNotification"),
                object: nil,
                userInfo:["title":"Sorry", "message": "Error. Please try again!"])
            }
        } else {
            StoreManager.shared.restoreAllPurchases()
        }
    }
}


