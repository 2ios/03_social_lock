//
//  StackViewPass.swift
//  Social Lock
//
//  Created by User543 on 04.07.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit

class StackViewPass: UIStackView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    override func layoutSubviews() {
        super.layoutSubviews()
        for view in self.arrangedSubviews {
            view.radiusCorner(borderWidth: 1, borderColor: UIColor.white.cgColor, divider: 2)
        }
    }

}
