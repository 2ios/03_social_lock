//
//  ViewController.swift
//  Social Lock
//
//  Created by User543 on 09.06.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit

class StartViewController: UIViewController {

    var checkCode = ""
    var inputCode = ""
    var temp = ""
    var step: Int = 0
    var passwordMode: PasswordMode = .Check
    
    @IBOutlet weak var passLabel: UILabel!
    @IBOutlet weak var stackViewPass: UIStackView!
    @IBOutlet weak var onePassView: UIView!
    @IBOutlet weak var twoPassView: UIView!
    @IBOutlet weak var threePassView: UIView!
    @IBOutlet weak var fourPassView: UIView!
    
    @IBOutlet weak var tapButtonOutlet1: UIButton!
    @IBOutlet weak var tapButtonOutlet2: UIButton!
    @IBOutlet weak var tapButtonOutlet3: UIButton!
    @IBOutlet weak var tapButtonOutlet4: UIButton!
    @IBOutlet weak var tapButtonOutlet5: UIButton!
    @IBOutlet weak var tapButtonOutlet6: UIButton!
    @IBOutlet weak var tapButtonOutlet7: UIButton!
    @IBOutlet weak var tapButtonOutlet8: UIButton!
    @IBOutlet weak var tapButtonOutlet9: UIButton!
    @IBOutlet weak var tapButtonOutlet0: UIButton!
    
    @IBOutlet weak var cancelDeleteButtonOutlet: UIButton!
    
    @IBAction func tapButton(_ sender: UIButton) {
        inputCode = inputCode + (sender.titleLabel?.text)!
        checkPass()
        if inputCode.characters.count == 4 {
            switch passwordMode {
            case .Check:
                checkPassword(str: inputCode)
            case .Change:
                changePassword(str: inputCode)
            default:
                newPassword(str: inputCode)
            }
        }
    }
    
    @IBAction func cancelDeleteButton(_ sender: UIButton) {
        switch inputCode.characters.count {
        case 0:
            if passwordMode == .Change{
                dismiss(animated: true, completion: nil)
            }
        case 1:
            inputCode = String(inputCode.characters.dropLast())
            onePassView.backgroundColor = UIColor.clear
            cancelDeleteButtonOutlet.isEnabled = true
            cancelDeleteButtonOutlet.setTitle("Delete", for: .normal)
        case 2:
            inputCode = String(inputCode.characters.dropLast())
            twoPassView.backgroundColor = UIColor.clear
            cancelDeleteButtonOutlet.isEnabled = true
            cancelDeleteButtonOutlet.setTitle("Delete", for: .normal)
        case 3:
            inputCode = String(inputCode.characters.dropLast())
            threePassView.backgroundColor = UIColor.clear
            cancelDeleteButtonOutlet.isEnabled = true
            cancelDeleteButtonOutlet.setTitle("Delete", for: .normal)
        case 4:
            inputCode = String(inputCode.characters.dropLast())
            fourPassView.backgroundColor = UIColor.clear
            cancelDeleteButtonOutlet.isEnabled = true
            cancelDeleteButtonOutlet.setTitle("Delete", for: .normal)
        default:
            break
        }
        checkPass()
    }
    
    func checkPass() {
        switch inputCode.characters.count {
        case 0:
            if passwordMode == .Check || passwordMode == .New {
                cancelDeleteButtonOutlet.isEnabled = false
                cancelDeleteButtonOutlet.setTitle("", for: .normal)
            } else if passwordMode == .Change {
                cancelDeleteButtonOutlet.isEnabled = true
                cancelDeleteButtonOutlet.setTitle("Cancel", for: .normal)
            }
        case 1:
            onePassView.backgroundColor = UIColor.white
            cancelDeleteButtonOutlet.isEnabled = true
            cancelDeleteButtonOutlet.setTitle("Delete", for: .normal)
        case 2:
            twoPassView.backgroundColor = UIColor.white
            cancelDeleteButtonOutlet.isEnabled = true
            cancelDeleteButtonOutlet.setTitle("Delete", for: .normal)
        case 3:
            threePassView.backgroundColor = UIColor.white
            cancelDeleteButtonOutlet.isEnabled = true
            cancelDeleteButtonOutlet.setTitle("Delete", for: .normal)
        case 4:
            fourPassView.backgroundColor = UIColor.white
            cancelDeleteButtonOutlet.isEnabled = true
            cancelDeleteButtonOutlet.setTitle("Delete", for: .normal)
        default:
            break
        }
    }
    func checkPassword(str: String) {
        if inputCode == checkCode {
            dismiss(animated: false, completion: nil)
        } else {
            stackViewPass.shake()
        }
        inputCode = ""
        delay()
    }
    
    func delay() {
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(100) ) {
            self.onePassView.backgroundColor = UIColor.clear
            self.twoPassView.backgroundColor = UIColor.clear
            self.threePassView.backgroundColor = UIColor.clear
            self.fourPassView.backgroundColor = UIColor.clear
        }
    }
    
    func changePassword(str: String) {
        if step == 0 {
            passLabel.text = "Enter new passcode"
            if inputCode == checkCode {
                step += 1
            } else {
                stackViewPass.shake()
                step = 0
                passLabel.text = "Enter old passcode"
            }
        } else if step == 1 {
            passLabel.text = "Enter renew passcode"
            step += 1
            temp = inputCode
        } else {
            if temp == inputCode {
                UserDefaults.standard.set(inputCode, forKey: "Password")
                let alert = UIAlertController(title: "Success", message: "Successfully changed passcode", preferredStyle: .alert)
                let action = UIAlertAction(title: "Dismiss", style: .cancel, handler: { succes in
                    self.dismiss(animated: true, completion: nil)
                })
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            } else {
                stackViewPass.shake()
                passLabel.text = "Enter old passcode"
            }
            step = 0
        }
        inputCode = ""
        delay()
        checkPass()
    }
    
    func newPassword(str: String) {
        if step == 0 {
            passLabel.text = "Enter renew passcode"
            temp = inputCode
            step += 1
        } else {
            if temp == inputCode {
                UserDefaults.standard.set(inputCode, forKey: "Password")
                dismiss(animated: false, completion: nil)
            } else {
                stackViewPass.shake()
                passLabel.text = "Enter new passcode"
                step = 0
            }
        }
        inputCode = ""
        delay()
        checkPass()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        switch passwordMode {
        case .Check:
            passLabel.text = "Enter passcode"
        case .Change:
            passLabel.text = "Enter old passcode"
            cancelDeleteButtonOutlet.isEnabled = true
            cancelDeleteButtonOutlet.setTitle("Cancel", for: .normal)
        default:
            passLabel.text = "Enter new passcode"
            
        }
        let imageView = UIImageView(image: #imageLiteral(resourceName: "backgroundStartView"))
        imageView.frame.size.width = self.view.frame.width
        imageView.frame.size.height = self.view.frame.height
        self.view.addSubview(imageView)
        self.view.sendSubview(toBack: imageView)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let code: String? = UserDefaults.standard.object(forKey: "Password") as? String
        if let c = code {
            checkCode = c
        } else {
            checkCode = ""
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillLayoutSubviews() {
        
        tapButtonOutlet0.radiusCorner(borderWidth: 1, borderColor: UIColor.white.cgColor, divider: 4)
        tapButtonOutlet1.radiusCorner(borderWidth: 1, borderColor: UIColor.white.cgColor, divider: 4)
        tapButtonOutlet2.radiusCorner(borderWidth: 1, borderColor: UIColor.white.cgColor, divider: 4)
        tapButtonOutlet3.radiusCorner(borderWidth: 1, borderColor: UIColor.white.cgColor, divider: 4)
        tapButtonOutlet4.radiusCorner(borderWidth: 1, borderColor: UIColor.white.cgColor, divider: 4)
        tapButtonOutlet5.radiusCorner(borderWidth: 1, borderColor: UIColor.white.cgColor, divider: 4)
        tapButtonOutlet6.radiusCorner(borderWidth: 1, borderColor: UIColor.white.cgColor, divider: 4)
        tapButtonOutlet7.radiusCorner(borderWidth: 1, borderColor: UIColor.white.cgColor, divider: 4)
        tapButtonOutlet8.radiusCorner(borderWidth: 1, borderColor: UIColor.white.cgColor, divider: 4)
        tapButtonOutlet9.radiusCorner(borderWidth: 1, borderColor: UIColor.white.cgColor, divider: 4)
    }
    
    @IBOutlet weak var widthStackViewPass: NSLayoutConstraint!
    @IBOutlet weak var heightStackViewPass: NSLayoutConstraint!
    @IBOutlet weak var widthPassLabel: NSLayoutConstraint!
    @IBOutlet weak var heightPassLabel: NSLayoutConstraint!
    
    override func viewDidLayoutSubviews() {
        self.widthStackViewPass.constant = self.view.frame.width * 0.245
        self.heightStackViewPass.constant = self.stackViewPass.arrangedSubviews[0].frame.width
        self.widthPassLabel.constant = self.view.frame.width * 0.528
        self.heightPassLabel.constant = self.view.frame.height * 0.031
    }
}

enum PasswordMode {
    
    case New
    case Check
    case Change
}

extension UIView {
    func shake() {
        let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        animation.duration = 0.6
        animation.values = [-20.0, 20.0, -20.0, 20.0, -10.0, 10.0, -5.0, 5.0, 0.0 ]
        layer.add(animation, forKey: "shake")
    }
    
    func radiusCorner(borderWidth: CGFloat, borderColor: CGColor, divider: CGFloat) -> Void {
        self.backgroundColor = UIColor.clear
        self.layer.borderColor = borderColor
        self.layer.borderWidth = borderWidth
        self.layer.cornerRadius = self.frame.height / divider
    }
}

