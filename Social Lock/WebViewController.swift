//
//  WebViewController.swift
//  Social Lock
//
//  Created by User543 on 09.06.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit
import GoogleMobileAds

class WebViewController: UIViewController, UIWebViewDelegate, GADBannerViewDelegate {
    
    
    @IBOutlet weak var bannerHeight: NSLayoutConstraint!
    
    var urlAddress: String = ""
    let notificationName = Notification.Name("UnlockAction")
    
    @IBOutlet weak var webView: UIWebView!
    
    @IBOutlet weak var gadView: GADBannerView!
    @IBOutlet weak var gadViewButton: UIButton!
    
    @IBAction func removeAdsButton(_ sender: UIButton) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ShowRemoveAds"), object: nil)
    }
    
    @IBAction func backMenu(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func goBack(_ sender: UIBarButtonItem) {
        webView.goBack()
    }
    @IBAction func goForward(_ sender: UIBarButtonItem) {
        webView.goForward()
    }
    @IBAction func reloadWeb(_ sender: UIBarButtonItem) {
        webView.reload()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.

        let url = URL(string: urlAddress)
        let urlRequest = URLRequest(url: url!)
        webView.loadRequest(urlRequest)
        
        let request = GADRequest()
        request.testDevices = [kGADSimulatorID, "8f8c2fd6b38f74b1348d68cc7ac92234","865e6ef02d21d31fd38fdca391e839ab"]
        // MARK: Google Ads Key
        gadView.adUnitID = "ca-app-pub-3940256099942544/2934735716"
        gadView.rootViewController = self
        gadView.delegate = self
        gadView.load(GADRequest())
        
        NotificationCenter.default.addObserver(self, selector: #selector(checkSubscription), name: NSNotification.Name(rawValue: "CheckSubscription"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        checkSubscription()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewDidLayoutSubviews() {
        if UI_USER_INTERFACE_IDIOM() == .pad {
            self.bannerHeight.constant = 90
        }
    }
    
    func checkSubscription() {
        if StoreManager.shared.isPurchased(id: "\(StoreManager.shared.nonConsumablesProductsIds.first!)") {
            print("Product is purchased")
            hideAds()
        } else {
            print("Product is not purchased")
            showAds()
        }
    }
    
    func showAds() {
        gadView.isHidden = false
        gadViewButton.isHidden = false
    }
    
    func hideAds() {
        gadView.isHidden = true
        gadViewButton.isHidden = true
    }
    
    func delay() {
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(500) ) {
        }
    }
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
//        gadViewButton.isHidden = false
    }
}
